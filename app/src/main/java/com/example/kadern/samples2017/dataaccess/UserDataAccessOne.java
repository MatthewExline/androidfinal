package com.example.kadern.samples2017.dataaccess;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import com.example.kadern.samples2017.MySQLiteOpenHelper;
//import com.example.kadern.samples2017.models.Item;
import com.example.kadern.samples2017.models.User;

import java.util.ArrayList;
/**
 * Created by 003012387 on 11/15/2017.
 */

public class UserDataAccessOne {
    public static final String TAG = "UserDataAccess";

    MySQLiteOpenHelper dbHelper;
    SQLiteDatabase db;

    public static final String  TABLE_NAME = "users";
    public static final String  COLUMN_USER_ID = "_id";
    public static final String  COLUMN_USER_NAME = "userName";
    public static final String COLUMN_USER_EMAIL = "userEmail";
    public static final String COLUMN_USER_FAVMUSIC = "favoriteMusic";
    public static final String COLUMN_USER_ACTIVE = "userActive";

    public static final String TABLE_CREATE = String.format("create table %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT, %s TEXT, %s TEXT, %s BOOLEAN)",
            TABLE_NAME, COLUMN_USER_ID, COLUMN_USER_NAME,
            COLUMN_USER_EMAIL, COLUMN_USER_FAVMUSIC, COLUMN_USER_ACTIVE);

    public UserDataAccessOne(MySQLiteOpenHelper dbHelper){
        this.dbHelper = dbHelper;
        this.db = this.dbHelper.getWritableDatabase();
    }

    public User insertUser(User u){
        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_NAME, u.getFirstName());
        values.put(COLUMN_USER_EMAIL, u.getEmail());
        values.put(COLUMN_USER_FAVMUSIC, u.getFavoriteMusic().toString());
        values.put(COLUMN_USER_ACTIVE, u.isActive());
        long insertId = db.insert(TABLE_NAME, null, values);
        u.setId(insertId);
        return u;
    }
    public boolean returnBool(int i){
        if(i==0) return false;
        else return true;
    }

    public User getUserById(long id){
        String query = String.format("SELECT %s, %s, %s, %s, %s FROM %s WHERE _id = %s", COLUMN_USER_ID, COLUMN_USER_NAME, COLUMN_USER_EMAIL, COLUMN_USER_FAVMUSIC, COLUMN_USER_ACTIVE, TABLE_NAME, String.valueOf(id));
        Cursor c = db.rawQuery(query, null);
        User.Music x=User.Music.RAP;
        if(c.getString(3).equals("COUNTRY")){x= User.Music.COUNTRY;}
        else if(c.getString(3).equals("JAZZ")){x=User.Music.JAZZ;}
        return new User(c.getLong(0), c.getString(1), c.getString(2), x, returnBool(c.getInt(4)));
    }

    public ArrayList<User> getAllUsers(){
        ArrayList<User> users = new ArrayList<User>();
        String query = String.format("SELECT %s, %s, %s, %s, %s FROM %s", COLUMN_USER_ID, COLUMN_USER_NAME, COLUMN_USER_EMAIL, COLUMN_USER_FAVMUSIC, COLUMN_USER_ACTIVE, TABLE_NAME);
        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();

        while(!c.isAfterLast()){
            User.Music x=User.Music.RAP;
            if(c.getString(3).equals("COUNTRY")){x= User.Music.COUNTRY;}
            else if(c.getString(3).equals("JAZZ")){x=User.Music.JAZZ;}
            User u = new User(c.getLong(0), c.getString(1), c.getString(2), x, returnBool(c.getInt(4)));
            //                ID            NAME            EMAIL           MUSIC           ACTIVE?
            users.add(u);
            c.moveToNext();
        }
        c.close();
        return users;
    }

    public User updateUser(User u){
        //Log.d(TAG, "meow-woof");
        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_ID, u.getId());
        values.put(COLUMN_USER_NAME, u.getFirstName());
        values.put(COLUMN_USER_EMAIL, u.getEmail());
        values.put(COLUMN_USER_FAVMUSIC, u.getFavoriteMusic().toString());
        values.put(COLUMN_USER_ACTIVE, u.isActive());

        int rowsUpdated = db.update(TABLE_NAME, values, "_id = " + u.getId(), null);
        return u;
    }

    public int deleteUser(long id){
        int rowsDeleted = db.delete(TABLE_NAME, COLUMN_USER_ID + " = " + id, null);
        return rowsDeleted;
    }
}
