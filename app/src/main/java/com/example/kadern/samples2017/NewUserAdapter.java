package com.example.kadern.samples2017;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kadern.samples2017.models.User;

import java.util.ArrayList;

import static com.example.kadern.samples2017.R.id.userListView;

/**
 * Created by 003012387 on 12/4/2017.
 */

public class NewUserAdapter extends ArrayAdapter<User> implements View.OnClickListener {
    private ArrayList<User> userList;
    Context mContext;
    public String TAG = "NewUserAdapter";

    private static class ViewHolder{
        TextView txtName;
        TextView txtType;
        TextView txtVersion;
        ImageView info;
    }

    public NewUserAdapter(ArrayList<User> data, Context c){
        super(c, R.layout.activity_user_list, data);
        this.userList = data;
        this.mContext = c;
    }

    @Override
    public void onClick(View view) {
        int position=(Integer) view.getTag();
        Object object= getItem(position);
        User u = (User)object;
        switch(view.getId()){
            case userListView:
                Log.d(TAG, "You clicked?");
            break;
        }
    }
}