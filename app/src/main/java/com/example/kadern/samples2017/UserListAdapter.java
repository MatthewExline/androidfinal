package com.example.kadern.samples2017;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kadern.samples2017.models.User;

import java.util.ArrayList;

/**
 * Created by kadern on 10/23/2017.
 */

public class UserListAdapter extends ArrayAdapter {

    ArrayList<User> users;

    public UserListAdapter(Context context, int layoutResourceID, int x, ArrayList<User> users){
        super(context, layoutResourceID, x, users);
        this.users = users;
        //Toast.makeText(UserListAdapter.this, "marrrr!!!", Toast.LENGTH_SHORT).show();

    }

    @Override
    public View getView(int position, View view, ViewGroup parent){
        //Toast.makeText(UserListAdapter.this, "marrrr!!!", Toast.LENGTH_SHORT).show();
        View userView = super.getView(position, view, parent);
        TextView lblFirstName = (TextView)userView.findViewById(R.id.lblFirstName);
        CheckBox chkActive = (CheckBox)userView.findViewById(R.id.chkActive);

        User u = users.get(position);
        lblFirstName.setText(u.getFirstName());
        chkActive.setChecked(u.isActive());

        // TODO:
        // 1. chkActive.setTag(u)
        // 2. hook up the listener on the checkbox (and use getTag() in the callback)

        return userView;
    }

}
