package com.example.kadern.samples2017;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;
import com.example.kadern.samples2017.dataaccess.UserDataAccessOne;
import com.example.kadern.samples2017.models.User;
public class UserDetailsActivity extends AppCompatActivity {
    public static final String USER_ID_EXTRA = "userid";
    public static boolean CREATING_NEW = false;
    AppClass app;
    User user;
    EditText txtFirstName;
    EditText txtEmail;
    RadioGroup rgFavoriteMusic;
    CheckBox chkActive;
    Button btnSave;
    Button btnClearForm;
    Button btnDel;
    Button btnInsertData;
    UserDataAccessOne uda;
    MySQLiteOpenHelper helper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);
        app = (AppClass)getApplication();
        txtFirstName = (EditText)findViewById(R.id.txtFirstName);
        txtEmail = (EditText)findViewById(R.id.txtEmail);
        rgFavoriteMusic = (RadioGroup)findViewById(R.id.rgFavoriteMusic);
        chkActive = (CheckBox)findViewById(R.id.chkActive);
        btnSave = (Button)findViewById(R.id.btnSave);
        helper = new MySQLiteOpenHelper(this);
        uda = new UserDataAccessOne(helper);
        btnDel = (Button)findViewById(R.id.delUser);
        btnDel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if(user!=null) {
                   uda.deleteUser(user.getId());
                   Intent i = new Intent(UserDetailsActivity.this, UserListActivity.class);
                   startActivity(i);
                }else Toast.makeText(UserDetailsActivity.this, "We cannot delete a user during an attempt to create a user.", Toast.LENGTH_LONG).show();
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(user!=null) {
                    if (passesValidation(getDataFromUI())) {
                        uda.updateUser(getDataFromUI());
                        Intent i = new Intent(UserDetailsActivity.this, UserListActivity.class);
                        startActivity(i);
                    }
                }else Toast.makeText(UserDetailsActivity.this, "We cannot save during an attempt to create a user.", Toast.LENGTH_LONG).show();
            }
        });
        btnClearForm = (Button)findViewById(R.id.btnClearForm);
        btnClearForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetForm();
            }
        });

        btnInsertData = (Button) findViewById(R.id.btnInsertData);
        btnInsertData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (passesValidation(getDataFromUI())) {
                    uda.insertUser(getDataFromUI());
                    Intent i = new Intent(UserDetailsActivity.this, UserListActivity.class);
                    startActivity(i);
                }
            }
        });

        Intent i = getIntent();
        long userId = i.getLongExtra(USER_ID_EXTRA, -1);
        if(userId >= 0){
            user = app.getUserById(userId);
            putDataInUI();
        }
    }
    private boolean passesValidation(User u){
        boolean hasError = false;
        String msg = "";
        if(u.getFirstName()==null||u.getFirstName().equals("")) {
            msg+="It seems that you have forgotten to enter the user's name.\n";
            hasError=true;
        } if(u.getEmail()==null||u.getEmail().equals("")) {
            msg+="It seems that you have forgotten to enter the user's email.\n";
            hasError=true;
        } if(u.getFavoriteMusic()==null){
            msg+="It seems you have not set the favorite music.\n";
            hasError=true;
        }
        if(!hasError)
            return true;
        else{
            Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
            return false;
        }
    }
    private User getDataFromUI(){
        String firstName = txtFirstName.getText().toString();
        String email = txtEmail.getText().toString();
        boolean active = chkActive.isChecked();
        User.Music favoriteMusic = null;
        int selectedRadioButtonId = rgFavoriteMusic.getCheckedRadioButtonId();
        switch(selectedRadioButtonId){
            case R.id.rbCountry:
                favoriteMusic = User.Music.COUNTRY;
                break;
            case R.id.rbRap:
                favoriteMusic = User.Music.RAP;
                break;
            case R.id.rbJazz:
                favoriteMusic = User.Music.JAZZ;
                break;
        }
        if(user != null) {
            user.setFirstName(firstName);
            user.setActive(active);
            user.setEmail(email);
            user.setFavoriteMusic(favoriteMusic);
            return user;
        }else{
            return new User(1, firstName, email, favoriteMusic, active);
        }
    }

    private  void putDataInUI(){
        //Toast.makeText(this, user.toString(), Toast.LENGTH_LONG).show();
        if(user!=null) {
            txtFirstName.setText(user.getFirstName());
            txtEmail.setText(user.getEmail());
            chkActive.setChecked(user.isActive());
            switch (user.getFavoriteMusic()) {
                case COUNTRY:
                    rgFavoriteMusic.check(R.id.rbCountry);
                    break;
                case JAZZ:
                    rgFavoriteMusic.check(R.id.rbJazz);
                    break;
                case RAP:
                    rgFavoriteMusic.check(R.id.rbRap);
                    break;
            }
        }else{
            Toast.makeText(this, "The user you are trying to display has not yet been instantiated.", Toast.LENGTH_LONG).show();
        }
    }

    private void resetForm(){
        rgFavoriteMusic.clearCheck();
        txtFirstName.setText("");
        txtEmail.setText("");
        chkActive.setChecked(false);
    }
}
