package com.example.kadern.samples2017;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.kadern.samples2017.dataaccess.ItemDataAccess;
import com.example.kadern.samples2017.dataaccess.UserDataAccessOne;

/**
 * Created by kadern on 11/13/2017.
 */

public class MySQLiteOpenHelper extends SQLiteOpenHelper {

    private static final String TAG = "MySQLiteOpenHelper";
    private static final String DATA_BASE_NAME = "users.db";
    private static final int DATABASE_VERSION = 1;

    public MySQLiteOpenHelper(Context context) {
        super(context, DATA_BASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //String sql = ItemDataAccess.TABLE_CREATE;
        //db.execSQL(sql);
        String sqlUsers = UserDataAccessOne.TABLE_CREATE;
        db.execSQL(sqlUsers);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
