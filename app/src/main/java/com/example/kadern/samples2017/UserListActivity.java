package com.example.kadern.samples2017;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kadern.samples2017.dataaccess.UserDataAccessOne;
import com.example.kadern.samples2017.models.User;

public class UserListActivity extends AppCompatActivity {
    public static final String TAG = "UserListActivity";
    AppClass app;
    ListView userListView;
    Button addNewUser;
    UserDataAccessOne uda1;
    MySQLiteOpenHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);

        helper = new MySQLiteOpenHelper(this);
        uda1 = new UserDataAccessOne(helper);
        app = (AppClass)getApplication();
        app.users = uda1.getAllUsers();
        userListView = (ListView)findViewById(R.id.userListView);
        addNewUser = (Button)findViewById(R.id.addNewUser);
        addNewUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openUserDetailsActivity();
            }
        });
        ArrayAdapter<User> adapter = new ArrayAdapter<User>(this, android.R.layout.simple_list_item_1, app.users);
        userListView.setAdapter(adapter);
        userListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(UserListActivity.this, app.users, Toast.LENGTH_SHORT).show();
                User selectedUser = app.users.get(position);
                //Toast.makeText(UserListActivity.this, selectedUser.getFirstName(),Toast.LENGTH_SHORT).show();
                Intent i = new Intent(UserListActivity.this, UserDetailsActivity.class);
                i.putExtra(UserDetailsActivity.USER_ID_EXTRA, selectedUser.getId());
                //Toast.makeText(UserListActivity.this, "meow "+String.valueOf(id), Toast.LENGTH_SHORT).show();
                startActivity(i);
            }
        });
    }

    private void openUserDetailsActivity(){
        //startActivity(new Intent(UserListActivity.this, UserDetailsActivity.class));
        Intent i = new Intent(UserListActivity.this, UserDetailsActivity.class);
        //i.putExtra(String.valueOf(UserDetailsActivity.CREATING_NEW), true);
        startActivity(i);
    }
}
