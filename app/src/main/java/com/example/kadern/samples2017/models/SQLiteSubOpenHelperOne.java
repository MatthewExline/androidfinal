package com.example.kadern.samples2017.models;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.kadern.samples2017.MySQLiteOpenHelper;
import com.example.kadern.samples2017.dataaccess.ItemDataAccess;

/**
 * Created by 003012387 on 11/15/2017.
 */

public class SQLiteSubOpenHelperOne extends SQLiteOpenHelper {
    private static final String TAG = "SQLiteSubOpenHelperOne";
    private static final String DATA_BASE_NAME = "users.db";
    private static final int DATABASE_VERSION = 1;

    public SQLiteSubOpenHelperOne(Context context) {
        super(context, DATA_BASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = ItemDataAccess.TABLE_CREATE;
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
/*
str = "JAZZ"
userMusic um = null;
if(str.equals("JAZZ"){
    um = userMusic.JAZZ;
}
 */
